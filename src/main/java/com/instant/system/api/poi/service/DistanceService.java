package com.instant.system.api.poi.service;

import org.springframework.stereotype.Service;

@Service
public class DistanceService {

	/**
	 * Calculate the distance between two points (given the latitude/longitude of those points).
	 * Inspired from https://www.geodatasource.com/developers/java
	 * @param lat1
	 * @param lon1
	 * @param lat2
	 * @param lon2
	 * @return the distance in meters
	 */
	public double calculateDistance(double lat1, double lon1, double lat2, double lon2) {
		if ((lat1 == lat2) && (lon1 == lon2)) {
			return 0;
		} else {
			double theta = lon1 - lon2;
			double dist = Math.sin(Math.toRadians(lat1)) * Math.sin(Math.toRadians(lat2))
					+ Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) * Math.cos(Math.toRadians(theta));
			dist = Math.acos(dist);
			dist = Math.toDegrees(dist);
			dist = dist * 60 * 1.1515;
			dist = dist * 1.609344;
			dist = dist * 1000;
			return dist;
		}
	}
}
