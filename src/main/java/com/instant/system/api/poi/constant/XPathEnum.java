package com.instant.system.api.poi.constant;

public enum XPathEnum {
	
	LOWER_CORNER("boundedBy/Envelope/lowerCorner"),
	UPPER_CORNER("boundedBy/Envelope/upperCorner"),
	FEATURE_MEMBER("featureMember"), 
	CI_VCUB_P("./CI_VCUB_P"), 
    NOM("./NOM/text()"), 
    NBPLACES("./NBPLACES/text()"),
    NBVELOS("./NBVELOS/text()"), 
    POS("./geometry/Point/pos/text()");
 
    private String value;
 
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	XPathEnum(String value) {
        this.value = value;
    }
     
}