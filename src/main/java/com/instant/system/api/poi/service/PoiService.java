package com.instant.system.api.poi.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.instant.system.api.poi.constant.XPathEnum;
import com.instant.system.api.poi.resource.PointOfInterest;

@Service
public class PoiService {

	@Autowired
	DistanceService distanceService;

	@Value("${api.url}")
	private String apiUrl;

	private final Logger logger = LoggerFactory.getLogger(PoiService.class);

	private static final String LAT = "lat";
	private static final String LON = "lon";

	private Map<String, XPath> compiledXPaths = new HashMap<>();

	/**
	 * Search for Points Of Interest into the given XML Document
	 * @param document The XML document
	 * @return a list of PointOfInterest objects
	 */
	public List<PointOfInterest> find(Document document) {

		List<PointOfInterest> poiList = null;

		// Document not found
		if (document == null) {
			logger.error("Document not found");
			return poiList;
		}

		try {

			// Get the root element
			Element root = document.getDocumentElement();
			root.normalize();

			// Compile all the required xPaths for optimization purpose
			compiledXPaths = getCompiledXPaths();

			// VLS
			NodeList nodeList = getNodes(XPathEnum.FEATURE_MEMBER, root);

			if (nodeList != null) {

				// Get GPS positions
				Map<String, Double> gpsPositions = getBoundedByGpsPositions(root);

				poiList = new ArrayList<>();

				// Loop over all available nodes
				for (int i = 0; i < nodeList.getLength(); i++) {

					Node node = nodeList.item(i);

					if (node.getNodeType() == Node.ELEMENT_NODE) {

						Node ci = getNode(XPathEnum.CI_VCUB_P, node);

						if (ci != null) {
							poiList.add(buildPointOfInterest(ci, gpsPositions));
						}
					}
				}

				// Sort POIs by distance
				Collections.sort(poiList);
			}

		} catch (XPathExpressionException e) {
			logger.error(e.getMessage());
			e.printStackTrace();

		}

		return poiList;
	}

	/**
	 * Get the list of child of a given node 
	 * @param xPathEnum
	 * @param parentNode
	 * @return a list of node
	 * @throws XPathExpressionException
	 */
	private NodeList getNodes(XPathEnum xPathEnum, Node parentNode) throws XPathExpressionException {
		XPath xPath = compiledXPaths.get(xPathEnum.name());
		return xPath != null ? (NodeList) xPath.evaluate(xPathEnum.getValue(), parentNode, XPathConstants.NODESET)
				: null;
	}

	/**
	 * Get a specific node from a given node
	 * @param xPathEnum
	 * @param parentNode
	 * @return a node
	 * @throws XPathExpressionException
	 */
	private Node getNode(XPathEnum xPathEnum, Node parentNode) throws XPathExpressionException {
		XPath xPath = compiledXPaths.get(xPathEnum.name());
		return xPath != null ? (Node) xPath.evaluate(xPathEnum.getValue(), parentNode, XPathConstants.NODE) : null;
	}

	/**
	 * Get a node value as String
	 * @param xPathEnum
	 * @param parentNode
	 * @return a string value if found or null instead
	 * @throws XPathExpressionException
	 */
	private String getNodeAsString(XPathEnum xPathEnum, Node parentNode) throws XPathExpressionException {
		XPath xPath = compiledXPaths.get(xPathEnum.name());
		return xPath != null ? (String) xPath.evaluate(xPathEnum.getValue(), parentNode, XPathConstants.STRING) : null;
	}

	/**
	 * Get a node value as int
	 * @param xPathEnum
	 * @param parentNode
	 * @return a int value if found or 0 instead
	 * @throws XPathExpressionException
	 */
	private int getNodeAsInteger(XPathEnum xPathEnum, Node parentNode) throws XPathExpressionException {
		XPath xPath = compiledXPaths.get(xPathEnum.name());
		return xPath != null
				? ((Double) xPath.evaluate(xPathEnum.getValue(), parentNode, XPathConstants.NUMBER)).intValue()
				: 0;
	}

	/**
	 * Build a PointOfInterest object from a given Node and GPS positions.
	 * @param parentNode
	 * @param gpsPositions
	 * @return a PointOfInterest object
	 * @throws XPathExpressionException
	 * @throws NumberFormatException
	 */
	private PointOfInterest buildPointOfInterest(Node parentNode, Map<String, Double> gpsPositions)
			throws XPathExpressionException, NumberFormatException {

		PointOfInterest poi = new PointOfInterest();
		String nom = getNodeAsString(XPathEnum.NOM, parentNode);
		poi.setNom(nom);

		int nbPlaces = getNodeAsInteger(XPathEnum.NBPLACES, parentNode);
		poi.setNbPlaces(nbPlaces);

		int nbVelos = getNodeAsInteger(XPathEnum.NBVELOS, parentNode);
		poi.setNbVelos(nbVelos);

		String positionGPS = getNodeAsString(XPathEnum.POS, parentNode);
		poi.setPosition(positionGPS);

		String[] espg = split(positionGPS);
		Double latN = Double.valueOf(espg[0]);
		Double lonN = Double.valueOf(espg[1]);

		// Calculate and set the distance to the POI
		Double lat = gpsPositions.get(LAT);
		Double lon = gpsPositions.get(LON);

		Double distance = distanceService.calculateDistance(lat, lon, latN, lonN);
		poi.setDistance(distance);

		return poi;
	}

	/**
	 * Build the GPS positions from the BoundedBy node of Document root
	 * @param root
	 * @return 
	 * @throws XPathExpressionException
	 */
	private Map<String, Double> getBoundedByGpsPositions(Element root) throws XPathExpressionException {
		Map<String, Double> gpsPosition = new HashMap<>();

		// Get lowerCorner positions
		String epsg = getNodeAsString(XPathEnum.LOWER_CORNER, root);
		String[] espgTabLower = split(epsg);
		Double latLower = Double.valueOf(espgTabLower[0]);
		Double lonLower = Double.valueOf(espgTabLower[1]);

		// Get upperCorner positions
		epsg = getNodeAsString(XPathEnum.UPPER_CORNER, root);
		String[] espgTabUpper = split(epsg);
		Double latUpper = Double.valueOf(espgTabUpper[0]);
		Double lonUpper = Double.valueOf(espgTabUpper[1]);

		// Get middle of positions
		Double lat = (latLower + latUpper) / 2;
		Double lon = (lonLower + lonUpper) / 2;

		gpsPosition.put("lat", lat);
		gpsPosition.put("lon", lon);

		return gpsPosition;
	}

	/**
	 * Split a string containing the space char
	 * @param epsg
	 * @return a String table
	 */
	private String[] split(String epsg) {
		return epsg.split(" ");
	}

	/**
	 * Compile all the necessary xPath and store in a Map object
	 * @return a map object. Key is the node name and value is the related xpath object
	 */
	private Map<String, XPath> getCompiledXPaths() {
		Map<String, XPath> xPathMap = new HashMap<>();
		XPathFactory xpf = XPathFactory.newInstance();
		Arrays.asList(XPathEnum.values()).forEach(xPathEnum -> {
			XPath xPath = xpf.newXPath();
			try {
				xPath.compile(xPathEnum.getValue());
			} catch (XPathExpressionException e) {
				logger.error(e.getMessage());
				e.printStackTrace();
			}
			xPathMap.put(xPathEnum.name(), xPath);
		});

		return xPathMap;
	}
}