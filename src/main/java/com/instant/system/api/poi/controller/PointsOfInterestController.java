package com.instant.system.api.poi.controller;

import java.io.IOException;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.instant.system.api.poi.resource.PointOfInterest;
import com.instant.system.api.poi.service.PoiService;

@RestController
@RequestMapping("/poi")
public class PointsOfInterestController {

	@Autowired
	PoiService poiService;

	@Value("${api.url}")
	private String apiUrl;

	/**
	 * Entry point of service call 
	 * @param position a GPS position
	 * @return a ResponseEntity object resource object
	 */
	@GetMapping("/vls/{position}")
	public ResponseEntity<List<PointOfInterest>> collectionList(@PathVariable String position) {

		final String URL = getApiURL(position);
		Document document = getDocument(URL);
		List<PointOfInterest> poiResource = poiService.find(document);

		if (poiResource == null) {
			return ResponseEntity.notFound().build();
		} else {
			return ResponseEntity.ok(poiResource);
		}
	}

	/**
	 * Build the service url to call
	 * @param position
	 * @return
	 */
	private String getApiURL(String position) {
		StringBuffer strBuffer = new StringBuffer();
		strBuffer.append(apiUrl);
		strBuffer.append(position);
		return strBuffer.toString();
	}

	/**
	 * Get a Document object from a given URL
	 * @param URL
	 * @return
	 */
	private Document getDocument(String URL) {
		Document document = null;
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			document = builder.parse(URL);
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return document;
	}
}
