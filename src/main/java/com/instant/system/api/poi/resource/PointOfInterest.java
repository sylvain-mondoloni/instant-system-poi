package com.instant.system.api.poi.resource;

public class PointOfInterest implements Comparable<PointOfInterest> {

	private String nom;

	private String position;
	
	private Double distance;

	private int nbVelos;

	private int nbPlaces;
	
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public int getNbVelos() {
		return nbVelos;
	}

	public void setNbVelos(int nbVelos) {
		this.nbVelos = nbVelos;
	}

	public int getNbPlaces() {
		return nbPlaces;
	}

	public void setNbPlaces(int nbPlaces) {
		this.nbPlaces = nbPlaces;
	}

	public Double getDistance() {
		return distance;
	}

	public void setDistance(Double distance) {
		this.distance = distance;
	}
	
	@Override
	public int compareTo(PointOfInterest p) {
		return this.getDistance().compareTo(p.getDistance());
	}
	
}
