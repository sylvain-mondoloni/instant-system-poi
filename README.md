
## Description de l'API
L’API propose un web service qui prend en entrée une position GPS et retourne en sortie une liste triée de stations VLS, avec pour chaque station, son nom, sa position, sa distance, le nombre de vélos et de places disponibles.

---

## Prérequis
Installation et configuration d'un environnement JAVA / SPRING BOOT / MAVEN
Une fois packagée au format war, l'application doit etre dépoyée sur un serveur web, tel que TOMCAT.

## Outils utilisés
IDE Spring Tool Suite
Git

---

## Choix techniques
Le projet d'API est développé en JAVA et basé sur le framework SPRING BOOT. Ce framework facilite la conception d'application Web d'entreprise. Projet développé à partir de l'archetype SPRING BOOT https://start.spring.io/
Il s'agit d'un service REST qui retourne une liste de Vélos en Libre Service.
Ce service est disponible par appel d'une URL.
Ce service renvoie des données au format json, un format standard d'échange de données exploitables par n'importe quel client de cette API.

---

## Utilisation de l'API

Exemple d'URL du service pour la position GPS 4326
http://localhost:8080/poi/vls/4326

http://localhost:8080 : ici une représentation de l'url locale
/poi : racine du controller
/vls : mapping du service retournant la liste des Vélos en Libre Service (VLS)
/{position} (/4326) : paramètre position GPS

Le service renvoie un tableau d'objets JSON. Cette Liste est triée par distance, de la station la plus proche au la plus éloignée.
Exemple de VLS au format JSON
{
    "nom": "Grand Lebrun",
    "position": "44.851150 -0.608610",
    "distance": 649.5650622881757,
    "nbVelos": 2,
    "nbPlaces": 14
}

Nature des informations
    "nom": String: Le nom de la station
    "position": String: les coordonnées GPS
    "distance": Double: la distance en mètres qui sépare la position GPS de la station VLS
    "nbVelos": int: le nombre de vélo pouvant être stockés
    "nbPlaces": int: le nombre de places disponibles


---

## Améliorations possibles

1. Création d'un module maven commun qui effectuerait le traitement des Points d'intérets.
Il contiendrait le modèle de données et les services. Cette séparation de code permettrait de réutiliser cette brique logicielle dans un autre projet.

2. Optimisation du traitement XML. Comme il s'agit d'un traitement de flux XML conséquent, identifier une solution de parcours XML la moins coûteuse en terme de performance.
Mon choix s'est porté sur l'API XPath. Dans ce souci de performance, j'ai compilé les xPath nécessaires avant de boucler sur les élements de type Points d'interet du document XML.

3. Gestion des exceptions personnalisées
Dans le cas d'une erreur, renvoyer une structure de données compréhensible et exploitable par l'application consommatrice du service. 
On pourrait y faire figurer la nature et le code de l'erreur par exemple.

4. Accès sécurisé. Si l'API devait être en accès privé, mettre en place un système d'authentification sécurisé autorisant l'appel au service.

## Difficultés rencontrées

Calcul d'une distance entre deux points d'après leurs coordonnées GPS. Choix et intégration d'une API existante https://www.geodatasource.com/developers/java

---

## Organisation du temps
Choix techniques et installation de l'environnement : 0.5j

Implémentation
    Controller et service : 1.5j
    Tests unitaires: 0.5j

Refactoring: 0.25j
Rédaction de documentation: 0.25j
